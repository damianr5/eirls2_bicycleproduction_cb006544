import React, {Component} from "react";

class Login extends Component{

    constructor(props){
        super(props);
        this.state = {username: '', password: ''};
        this.changeHandler = this.changeHandler.bind(this);
        this.userLoginHandler = this.userLoginHandler.bind(this);

    }

    changeHandler = event=>
        this.setState({[event.target.name]: event.target.value});

    userLoginHandler(event){
        event.preventDefault();
        const data = new FormData(event.target);
        fetch("http://localhost:8080/adminLogin",{
            method: "POST",
            body: data
        })
            .then(response =>{
                if(response.status === 200)
                {
                    response.json().then(userId =>{
                        localStorage.setItem("userId", userId);
                        window.location.href = "/Dashboard";
                    });
                }else{
                    alert("Incorrect username or password!");
                    window.location.href="/";
                }
            });
    }

        render() {
            return (
                <div>
                    {/* Required meta tags*/}
                    <meta charSet="UTF-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
                    <meta name="description" content="au theme template" />
                    <meta name="author" content="Hau Nguyen" />
                    <meta name="keywords" content="au theme template" />
                    {/* Title Page*/}
                    <title>Login</title>

                    <div className="page-wrapper">
                        <div className="page-content--bge5">
                            <div className="container">
                                <div className="login-wrap">
                                    <div className="login-content">
                                        <div className="login-logo">
                                            <a>
                                                <img src="images/icon/production_logo.png" alt="CoolAdmin" />
                                            </a>
                                        </div>
                                        <div className="login-form">
                                            <form onSubmit={this.userLoginHandler}>
                                                <div className="form-group">
                                                    <label>Username</label>
                                                    <input className="au-input au-input--full" type="text" name="username" placeholder="Username"  value={this.state.username} onChange={this.changeHandler} />
                                                </div>
                                                <div className="form-group">
                                                    <label>Password</label>
                                                    <input className="au-input au-input--full" type="password" name="password" placeholder="Password" value={this.state.password} onChange={this.changeHandler} required />
                                                </div>
                                                <div className="login-checkbox">
                                                    <label>
                                                        <input type="checkbox" name="remember" />Remember Me
                                                    </label>
                                                </div>
                                                <button className="au-btn au-btn--block au-btn--green m-b-20" type="submit">Log in</button>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
}

export default Login;