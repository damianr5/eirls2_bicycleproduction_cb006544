import React, {Component} from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';
import Login from './Login';
import Dashboard from "./Dashboard";
import Schedule from "./Schedule";
import UpdateOrder from "./UpdateOrder";

class App extends Component {
  render() {
    return (
        <Router>
          <Switch>
            <Route path='/' exact={true} component={Login}/>
            <Route path='/dashboard' exact={true} component={Dashboard}/>
              <Route path='/updateorder' exact={true} component={UpdateOrder}/>
              <Route path='/schedule' exact={true} component={Schedule}/>
          </Switch>
        </Router>
    )
  }
}

export default App;