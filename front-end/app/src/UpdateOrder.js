import React, {Component} from "react";

class UpdateOrder extends Component {
    orderInfo={
        order_id:'',
        order_due_date:'',
        order_placement_date:'',
        order_status:'',
        bom_status:''
    };

    constructor(props){
        super(props);
        this.state = {item: this.orderInfo};
        this.state = {id: this.props.location.state.id};
        this.onChangeHandler = this.onChangeHandler.bind(this);
        this.handleOrderUpdate = this.handleOrderUpdate.bind(this);
        this.handleBOMB = this.handleBOMB.bind(this);
    }
    componentDidMount() {
            fetch(`http://localhost:8080/salesOrders/${this.props.location.state.id}`)
                .then(response=>{
                    return response.json();
                }).then(data=>{
                    console.log(data);
                    this.setState({item:data});


            })
    }

    onChangeHandler(event){
        let item = this.state.item;
        item[event.target.name] = event.target.value;
        this.setState({ item });
    }

    handleOrderUpdate(event){
        event.preventDefault();
        const data = new FormData(event.target);
        console.log(data);
        console.log(this.state.item);
        const orders = {
            id: this.state.item.order_id,
            order_status: data.get("order_status"),
            order_due_date: data.get("order_due_date")
        };
        fetch("http://172.20.10.13:8080/orders/productionOrder", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(orders)
        }).then(response=> {
            alert("Order Successfully Updated!");
            return response.json();
        }).then(data=>{
            console.log(data)
        });
        const salesOrders = {
            order_id: this.state.item.order_id,
            order_status: data.get("order_status"),
            order_due_date: data.get("order_due_date")
        };
        fetch("http://localhost:8080/salesOrders/salesOrdersUpdate",{
            method:"POST",
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body:JSON.stringify(salesOrders)
        }).then(response=>{
            alert("Order successfully Saved!");
        });
    }

    handleBOMB(event){
        event.preventDefault();
        console.log( "item",this.state.item.salesItems)
        const billOfMaterials = {
            order_Id: this.state.item.order_id,
            status: "Pending",
            billItems: this.state.item.salesItems
        };

        fetch("https://us-central1-material-management-f3b68.cloudfunctions.net/bom/newBOM",{
            method:"POST",
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body:JSON.stringify(billOfMaterials)
        }).then(response=>{
            alert("Bill of Materials Successfully Generated! (Imesh)");
        }).catch(error=>{
            console.log(error)
        })

        fetch("http://172.20.10.11:8080/billOfMaterials",{
            method:"POST",
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body:JSON.stringify(billOfMaterials)
        }).then(response=>{
            alert("Bill of Materials Successfully Generated! (Dhanuka)");
        });

        fetch("http://localhost:8080/bill_of_materials",{
            method:"POST",
            headers:{
                'Accept':'application/json',
                'Content-Type':'application/json'
            },
            body:JSON.stringify(billOfMaterials)
        }).then(response=>{
            alert("Bill of Materials Successfully Saved!")
        })


    }

    render(){
        const {item} = this.state;
        console.log(item);
        let saleItemList=[];
        if(item!==undefined){
            const orderItemList = item.salesItems;

            saleItemList = orderItemList!==undefined && orderItemList.map(orderItem=>{
                console.log(orderItem)
                return (
                    <tr>
                    <td>{orderItem.id}</td>
                    <td>{orderItem.prodId}</td>
                    <td>{orderItem.prodName}</td>
                    <td>{orderItem.qty}</td>
                    <td>{orderItem.status}</td>
                    </tr>
                )

            });
        }

        const orders = this.state.item;
        console.log(orders)
        return (
            <div>
                {/* Required meta tags*/}
                <meta charSet="UTF-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
                <meta name="description" content="au theme template"/>
                <meta name="author" content="Hau Nguyen"/>
                <meta name="keywords" content="au theme template"/>
                {/* Title Page*/}
                <title>Dashboard</title>

                <div className="page-wrapper">
                    {/* MENU SIDEBAR*/}
                    <aside className="menu-sidebar d-none d-lg-block">
                        <div className="logo">
                            <a href="#">
                                <img src="images/icon/production_logo.png" alt="Cool Admin"/>
                            </a>
                        </div>
                        <div className="menu-sidebar__content js-scrollbar1">
                            <nav className="navbar-sidebar">
                                <ul className="list-unstyled navbar__list">
                                    <li>
                                        <a className="js-arrow" href="/dashboard">
                                            <i className="fas fa-tachometer-alt"/>Orders</a>
                                    </li>
                                    <li>
                                        <a href="/schedule">
                                            <i className="fas fa-calendar "/>Schedule</a>
                                    </li>

                                </ul>
                            </nav>
                        </div>
                    </aside>
                    {/* END MENU SIDEBAR*/}
                    {/* PAGE CONTAINER*/}
                    <div className="page-container">
                        {/* HEADER DESKTOP*/}
                        <header className="header-desktop">
                            <div className="section__content section__content--p30">
                                <div className="container-fluid">
                                    <div className="header-wrap">
                                        <form className="form-header" action method="POST">
                                            <input className="au-input au-input--xl" type="text" name="search"
                                                   placeholder="Search for datas & reports..."/>
                                            <button className="au-btn--submit" type="submit">
                                                <i className="zmdi zmdi-search"/>
                                            </button>
                                        </form>
                                        <div className="header-button">
                                            <div className="noti-wrap">
                                            </div>
                                            <div className="account-wrap">
                                                <div className="account-item clearfix js-item-menu">
                                                    <div className="image">
                                                        <img src="images/icon/avatar-01.jpg" alt="John Doe"/>
                                                    </div>
                                                    <div className="content">
                                                        <a className="js-acc-btn">john doe</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </header>
                        {/* HEADER DESKTOP*/}
                        {/* MAIN CONTENT*/}
                        <div className="main-content">
                            <div className="section__content section__content--p30">
                                <div className="container-fluid">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="overview-wrap">
                                                <h2 className="title-1">update orders</h2>
                                                {item!==undefined && item.order_status === "Order"?(
                                                <button  className="au-btn au-btn-icon au-btn--green" onClick={this.handleBOMB}>
                                                    <i className="zmdi zmdi-archive"></i>Generate BOM
                                                </button>
                                                ):("")
                                                }
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row2">
                                        <div className="col-lg-12">
                                            <div className="table-responsive table--no-card m-b-40">
                                                <table className="table table-borderless table-striped table-earning">
                                                    <thead>
                                                    <tr>
                                                        <th>Order ID</th>
                                                        <th>Product ID</th>
                                                        <th>Product Name</th>
                                                        <th>Quantity</th>
                                                        <th>Status</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    {saleItemList}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    {
                                        orders !== undefined ? (
                                    <div className="row2">
                                        <div className="col-lg-12">
                                            <form onSubmit={this.handleOrderUpdate}>
                                            <div className="card">
                                                <div className="card-body card-block">
                                                    <div className="form-group">
                                                        <label htmlFor="id" className=" form-control-label">Order
                                                            ID</label>
                                                        <input type="text" disabled
                                                               placeholder={orders.order_id}
                                                               className="form-control"/>
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="placement_date" className=" form-control-label">Order
                                                            Placement date</label>
                                                        <input type="text" disabled placeholder={orders.order_placement_date}
                                                               className="form-control"/>
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="due_date" className=" form-control-label">Order
                                                            Due Date</label>
                                                        <input type="date" placeholder="Enter Due date" name="order_due_date" value={orders.order_due_date} onChange={this.onChangeHandler}
                                                               className="form-control"/>
                                                    </div>
                                                    <div className="row form-group">
                                                        <div className="col-8">
                                                            <div className="form-group">
                                                                <label htmlFor="status"
                                                                       className=" form-control-label">Order Status</label>
                                                                <input type="text" value={orders.order_status} name="order_status"
                                                                       onChange={this.onChangeHandler} className="form-control"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <button type="submit"
                                                            className="btn btn-lg btn-info btn-block">
                                                        <span id="payment-button-amount">Update</span>
                                                    </button>
                                                </div>
                                            </div>
                                            </form>
                                        </div>

                                    </div>
                                        ) : ("")}
                                </div>
                            </div>
                        </div>
                        {/* END MAIN CONTENT*/}
                        {/* END PAGE CONTAINER*/}
                    </div>
                </div>
            </div>
        );
    }
}

export default UpdateOrder;