import React, {Component} from "react";
import './App.css';

class Schedule extends Component {

    constructor(props) {
        super(props);
        this.state = {
            orders: [],
            selectedOrder: {},
            order_id:"",
            schedules:[]
        };
        this.changeOrder = this.changeOrder.bind(this);
        this.handleSchedule = this.handleSchedule.bind(this);
    }

    componentDidMount() {
        fetch('http://localhost:8080/salesOrders')
            .then(response => response.json())
            .then(data => this.setState({orders: data}));

        fetch('http://localhost:8080/schedule')
            .then(response => response.json())
            .then(data => this.setState({schedules: data}));
    }

    changeOrder(event) {
        let orders = this.state.orders;
        console.log(orders);
        console.log(event.target.value);
        this.setState({
            selectedOrder: orders[event.target.value],
            order_id:orders[event.target.value].order_id
        })
    }

    handleSchedule(event) {

        event.preventDefault();
        const data = new FormData(event.target);

        const schedule = {
            order_id: this.state.order_id,
            start_date: data.get("start_date"),
            end_date: data.get("end_date"),
            employee_amount: data.get("employee_amount")
        }

        fetch("http://localhost:8080/schedule", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(schedule)
        }).then(response => {
            alert("Added Schedule");
            return response.json();
        });
    }

    render() {

        const {schedules} = this.state;
        console.log(schedules)
        const scheduleList = schedules!==undefined && schedules.map(schedule=>{
            return(
                <tr>
                    <td>{schedule.order_id}</td>
                    <td>{schedule.start_date}</td>
                    <td>{schedule.employee_amount}</td>
                    <td>{schedule.end_date}</td>
                </tr>
            )
        })

        const {orders} = this.state;

        return (
            <div>
                {/* Required meta tags*/}
                <meta charSet="UTF-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
                <meta name="description" content="au theme template"/>
                <meta name="author" content="Hau Nguyen"/>
                <meta name="keywords" content="au theme template"/>
                {/* Title Page*/}
                <title>Dashboard</title>

                <div className="page-wrapper">

                    {/* MENU SIDEBAR*/}
                    <aside className="menu-sidebar d-none d-lg-block">
                        <div className="logo">
                            <a href="#">
                                <img src="images/icon/production_logo.png" alt="Cool Admin"/>
                            </a>
                        </div>
                        <div className="menu-sidebar__content js-scrollbar1">
                            <nav className="navbar-sidebar">
                                <ul className="list-unstyled navbar__list">
                                    <li>
                                        <a className="js-arrow" href="/dashboard">
                                            <i className="fas fa-tachometer-alt"/>Orders</a>
                                    </li>
                                    <li className="active has-sub">
                                        <a href="/schedule">
                                            <i className="fas fa-calendar "/>Schedule</a>
                                    </li>

                                </ul>
                            </nav>
                        </div>
                    </aside>
                    {/* END MENU SIDEBAR*/}
                    {/* PAGE CONTAINER*/}
                    <div className="page-container">
                        {/* HEADER DESKTOP*/}
                        <header className="header-desktop">
                            <div className="section__content section__content--p30">
                                <div className="container-fluid">
                                    <div className="header-wrap">
                                        <form className="form-header" action method="POST">
                                            <input className="au-input au-input--xl" type="text" name="search"
                                                   placeholder="Search for datas & reports..."/>
                                            <button className="au-btn--submit" type="submit">
                                                <i className="zmdi zmdi-search"/>
                                            </button>
                                        </form>
                                        <div className="header-button">
                                            <div className="noti-wrap">
                                            </div>
                                            <div className="account-wrap">
                                                <div className="account-item clearfix js-item-menu">
                                                    <div className="image">
                                                        <img src="images/icon/avatar-01.jpg" alt="John Doe"/>
                                                    </div>
                                                    <div className="content">
                                                        <a className="js-acc-btn">john doe</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </header>
                        {/* HEADER DESKTOP*/}
                        {/* MAIN CONTENT*/}
                        <div className="main-content">
                            <div className="section__content section__content--p30">
                                <div className="container-fluid">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="overview-wrap">
                                                <h2 className="title-1">Scheduler</h2>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row2">
                                        <div className="col-lg-12">
                                            <form onSubmit={this.handleSchedule}>
                                                <div className="card">
                                                    <div className="card-body card-block">
                                                        <div className="form-group">
                                                            <label htmlFor="id" className=" form-control-label">Order
                                                                ID</label>
                                                            <select className="form-control" name="order_id"
                                                                    onChange={(event) => this.changeOrder(event)}>
                                                            {
                                                                this.state.orders.map((order, index) => {
                                                                    if(order.order_status==="Order"){
                                                                        return <option key={index}
                                                                                       value={index}>{order.order_id}</option>
                                                                    }
                                                                })
                                                            }
                                                        }
                                                            </select>
                                                        </div>
                                                        <div className="form-group">
                                                            <label htmlFor="start_date" className=" form-control-label">Start
                                                                order date</label>
                                                            <input type="date"
                                                                   name="start_date" className="form-control"/>
                                                        </div>
                                                        <div className="form-group">
                                                            <label htmlFor="employee_allocation"
                                                                   className=" form-control-label">Allocate number of
                                                                employees</label>
                                                            <input type="number"
                                                                   name="employee_amount" className="form-control"
                                                                   min="1" max="5"/>
                                                        </div>
                                                        <div className="form-group">
                                                            <label htmlFor="deadline"
                                                                   className=" form-control-label">Deadline</label>
                                                            <input disabled placeholder={orders.order_due_date}
                                                                   type="text" name="order_due_date"
                                                                   className="form-control"
                                                                   value={this.state.selectedOrder !== undefined && this.state.selectedOrder.order_due_date !== undefined ? (
                                                                       this.state.selectedOrder.order_due_date) : ("")}/>
                                                        </div>
                                                        <div className="form-group">
                                                            <label htmlFor="end_date" className=" form-control-label">End
                                                                order date</label>
                                                            <input type="date" name="end_date"
                                                                   className="form-control"/>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <button type="submit"
                                                                className="btn btn-lg btn-info btn-block">
                                                            <span id="payment-button-amount">Schedule Order</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                    <div className="row2">
                                        <div className="col-lg-12">
                                            <div className="table-responsive table--no-card m-b-40">
                                                <table className="table table-borderless table-striped table-earning">
                                                    <thead>
                                                    <tr>
                                                        <th>Order ID</th>
                                                        <th>Start Date</th>
                                                        <th>Number of employees allocated</th>
                                                        <th>End Date</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    {scheduleList}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* END MAIN CONTENT*/}
                        {/* END PAGE CONTAINER*/}
                    </div>
                </div>
            </div>
        );
    }
}

export default Schedule;