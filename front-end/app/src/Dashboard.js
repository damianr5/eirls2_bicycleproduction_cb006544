import React, {Component} from "react";
import {Redirect} from "react-router-dom";

class Dashboard extends Component{

    constructor(props){
        super(props);
        this.state={
            orders:[],
            redirectToOrderDetailed:false
        };
    }

    clickHandler(id) {
        console.log(id);
        this.setState(({
            orderId: id,
            redirectToOrderDetailed: true
        }))

    }

    componentDidMount() {
        fetch('http://localhost:8080/salesOrders')
            .then(response => response.json())
            .then(data=> this.setState({orders:data}));

    }

        render() {

        const {orders} = this.state;
        console.log(orders);
        const orderList = orders!==undefined && orders.map(order=>{
           return (
               <tr onClick={()=>this.clickHandler(order.id)}>
                   <td>{order.order_id}</td>
                   <td>{order.order_placement_date}</td>
                   <td>{order.order_due_date}</td>
                   <td>{order.order_status}</td>
               </tr>
           )
        });

            return (
                <div>
                    {
                        this.state.redirectToOrderDetailed?(
                            <Redirect to={{pathname:"/updateorder",state:{id:this.state.orderId}}}/>
                        ):("")
                    }
                    {/* Required meta tags*/}
                    <meta charSet="UTF-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
                    <meta name="description" content="au theme template" />
                    <meta name="author" content="Hau Nguyen" />
                    <meta name="keywords" content="au theme template" />
                    {/* Title Page*/}
                    <title>Dashboard</title>

                    <div className="page-wrapper">
                        {/* MENU SIDEBAR*/}
                        <aside className="menu-sidebar d-none d-lg-block">
                            <div className="logo">
                                <a href="#">
                                    <img src="images/icon/production_logo.png" alt="Cool Admin" />
                                </a>
                            </div>
                            <div className="menu-sidebar__content js-scrollbar1">
                                <nav className="navbar-sidebar">
                                    <ul className="list-unstyled navbar__list">
                                        <li className="active has-sub">
                                            <a className="js-arrow" href="dashboard">
                                                <i className="fas fa-tachometer-alt" />Orders</a>
                                        </li>
                                        <li>
                                            <a href="/schedule">
                                                <i className="fas fa-calendar " />Schedule</a>
                                        </li>

                                    </ul>
                                </nav>
                            </div>
                        </aside>
                        {/* END MENU SIDEBAR*/}
                        {/* PAGE CONTAINER*/}
                        <div className="page-container">
                            {/* HEADER DESKTOP*/}
                            <header className="header-desktop">
                                <div className="section__content section__content--p30">
                                    <div className="container-fluid">
                                        <div className="header-wrap">
                                            <form className="form-header" action method="POST">
                                                <input className="au-input au-input--xl" type="text" name="search" placeholder="Search for datas & reports..." />
                                                <button className="au-btn--submit" type="submit">
                                                    <i className="zmdi zmdi-search" />
                                                </button>
                                            </form>
                                            <div className="header-button">
                                                <div className="noti-wrap">
                                                </div>
                                                <div className="account-wrap">
                                                    <div className="account-item clearfix js-item-menu">
                                                        <div className="image">
                                                            <img src="images/icon/avatar-01.jpg" alt="John Doe" />
                                                        </div>
                                                        <div className="content">
                                                            <a className="js-acc-btn" >john doe</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </header>
                            {/* HEADER DESKTOP*/}
                            {/* MAIN CONTENT*/}
                            <div className="main-content">
                                <div className="section__content section__content--p30">
                                    <div className="container-fluid">
                                        <div className="row">
                                            <div className="col-md-12">
                                                <div className="overview-wrap">
                                                    <h2 className="title-1">orders</h2>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row2">
                                            <div className="col-lg-12">
                                                <div className="table-responsive table--no-card m-b-40">
                                                    <table className="table table-borderless table-striped table-earning">
                                                        <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>Placement Date</th>
                                                            <th>End Date</th>
                                                            <th>Status</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        {orderList}
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* END MAIN CONTENT*/}
                            {/* END PAGE CONTAINER*/}
                        </div>
                    </div>
                </div>
            );
        }
}

export default Dashboard;