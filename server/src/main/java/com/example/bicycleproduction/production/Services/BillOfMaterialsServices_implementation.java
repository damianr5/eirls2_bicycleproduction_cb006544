package com.example.bicycleproduction.production.Services;

import com.example.bicycleproduction.production.Models.BillItem;
import com.example.bicycleproduction.production.Models.BillOfMaterials;
import com.example.bicycleproduction.production.Repositories.BillItemRepository;
import com.example.bicycleproduction.production.Repositories.BillOfMaterialsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class BillOfMaterialsServices_implementation implements BillOfMaterialsServices {

    @Autowired
    private BillOfMaterialsRepository bill_of_materials_repo;

    @Autowired
    private BillItemRepository billItemRepo;

    @Override
    public BillOfMaterials saveBillOfMaterials(BillOfMaterials newBillOfMaterials) {
        BillOfMaterials billOfMaterials = bill_of_materials_repo.save(newBillOfMaterials);
        List<BillItem> billItems = newBillOfMaterials.getBillItems();
        for(BillItem bill : billItems){
            bill.setBom(billOfMaterials);
            billItemRepo.save(bill);
        }
        return billOfMaterials;
    }

    @Override
    public BillOfMaterials getBillOfMaterialsById(int id) {
        return bill_of_materials_repo.findById(id).orElse(null);
    }

    @Override
    public void deleteBillOfMaterials(int id) {
        BillOfMaterials user = bill_of_materials_repo.findById(id).orElse(null);
        bill_of_materials_repo.delete(user);
    }

    @Override
    public List<BillOfMaterials> getAllBillOfMaterials() {
        return bill_of_materials_repo.findAll();
    }
}



