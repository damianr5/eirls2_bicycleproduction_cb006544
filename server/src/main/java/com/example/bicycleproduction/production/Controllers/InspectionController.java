package com.example.bicycleproduction.production.Controllers;

import com.example.bicycleproduction.production.Models.Inspection;
import com.example.bicycleproduction.production.Services.InspectionServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/inspection")
public class InspectionController {
    @Autowired
    private InspectionServices inspectionService;

    @GetMapping
    public List<Inspection> getAllInspections() {
        return inspectionService.getAllInspections();
    }

    @GetMapping("/{id}")
    public Inspection getInspection(@PathVariable("id") int id) {
        return inspectionService.getInspectionById(id);
    }

    @PostMapping
    public Inspection createInspection(@RequestBody Inspection inspection) {
        return inspectionService.saveInspection(inspection);
    }

    @PutMapping
    public Inspection updateInspection(@RequestBody Inspection inspection) {
        return inspectionService.saveInspection(inspection);
    }

    @DeleteMapping("/{id}")
    public void deleteInspection(@PathVariable("id") int id) {
        inspectionService.deleteInspection(id);
    }
}

