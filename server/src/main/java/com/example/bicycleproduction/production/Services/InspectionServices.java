package com.example.bicycleproduction.production.Services;

import com.example.bicycleproduction.production.Models.Inspection;

import java.util.List;


public interface InspectionServices {

    Inspection saveInspection(Inspection inspection);

    Inspection getInspectionById(int id);

    void deleteInspection(int id);

    List<Inspection> getAllInspections();
}
