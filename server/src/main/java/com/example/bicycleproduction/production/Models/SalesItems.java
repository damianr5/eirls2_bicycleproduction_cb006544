package com.example.bicycleproduction.production.Models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class SalesItems {

    private @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer productId;

    private String id;
    private String prodId;
    private String prodName;
    private int qty;
    private String status;

    @ManyToOne
    private SalesOrders salesOrders;
}
