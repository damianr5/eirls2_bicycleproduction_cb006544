package com.example.bicycleproduction.production.Services;

import com.example.bicycleproduction.production.Models.SalesOrders;

import java.util.List;

public interface SalesOrdersServices {

    SalesOrders saveSalesOrders(SalesOrders salesOrders);

    SalesOrders getSalesOrdersById(Integer id);

    void deleteSalesOrders(Integer id);

    List<SalesOrders> getAllSalesOrders();
}
