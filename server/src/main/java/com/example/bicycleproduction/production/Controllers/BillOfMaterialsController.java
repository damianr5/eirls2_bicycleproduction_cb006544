package com.example.bicycleproduction.production.Controllers;

import com.example.bicycleproduction.production.Models.BillItem;
import com.example.bicycleproduction.production.Models.BillOfMaterials;
import com.example.bicycleproduction.production.Services.BillItemServices;
import com.example.bicycleproduction.production.Services.BillOfMaterialsServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/bill_of_materials")
@CrossOrigin("*")
public class BillOfMaterialsController {
    @Autowired
    private BillOfMaterialsServices billOfMaterialsService;

    @Autowired
    private BillItemServices billItemServices;

    @GetMapping
    public List<BillOfMaterials> getAllBillOfMaterials() {
        List<BillOfMaterials> itemList = billOfMaterialsService.getAllBillOfMaterials();
        List<BillOfMaterials> tempItemList = new ArrayList<>();
        for(BillOfMaterials item : itemList){
            tempItemList.add(filterBOM(item));
        }
        return tempItemList;
    }

    @GetMapping("/{id}")
    public BillOfMaterials getBillOfMaterials(@PathVariable("id") int id) {
        return billOfMaterialsService.getBillOfMaterialsById(id);
    }

    @PostMapping
    public BillOfMaterials createBillOfMaterials(@RequestBody BillOfMaterials billOfMaterials) {
        List<BillItem> billItems = billOfMaterials.getBillItems();
        List<BillItem> tempbillItems = new ArrayList<>();

        billOfMaterials = billOfMaterialsService.saveBillOfMaterials(billOfMaterials);
        for (BillItem items: billItems){
            items.setBom(billOfMaterials);
            items = billItemServices.saveBillItem(items);
            tempbillItems.add(items);
        }

        billOfMaterials.setBillItems(tempbillItems);
        billOfMaterials = filterBOM(billOfMaterials);

        return billOfMaterials;
    }

    public BillOfMaterials filterBOM(BillOfMaterials billOfMaterials){
        List<BillItem> billItems = billOfMaterials.getBillItems();
        List<BillItem> tempbillItems = new ArrayList<>();
        for (BillItem item: billItems) {
            item.setBom(null);
            tempbillItems.add(item);
        }
        billOfMaterials.setBillItems(tempbillItems);

        return billOfMaterials;
    }

    @PutMapping
    public BillOfMaterials updateBillOfMaterials(@RequestBody BillOfMaterials BillOfMaterials) {
        return billOfMaterialsService.saveBillOfMaterials(BillOfMaterials);
    }

    @DeleteMapping("/{id}")
    public void deleteBillOfMaterials(@PathVariable("id") int id) {
        billOfMaterialsService.deleteBillOfMaterials(id);
    }
}

