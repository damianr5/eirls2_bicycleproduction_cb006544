package com.example.bicycleproduction.production.Services;

import com.example.bicycleproduction.production.Models.SalesItems;
import com.example.bicycleproduction.production.Repositories.SalesItemsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SalesItemServiceImplementation implements SalesItemsService {

    @Autowired
    private SalesItemsRepo salesItemsRepo;

    @Override
    public SalesItems saveSalesItems(SalesItems salesItems) {
        return salesItemsRepo.save(salesItems);
    }

    @Override
    public void deleteSalesItems(Integer id) {
        SalesItems salesItems = salesItemsRepo.findById(id).orElse(null);
        salesItemsRepo.delete(salesItems);
    }

    @Override
    public SalesItems getSalesItemsById(Integer id) {
        return salesItemsRepo.findById(id).orElse(null);
    }

    @Override
    public List<SalesItems> getAllSalesItems() {
        return salesItemsRepo.findAll();
    }
}
