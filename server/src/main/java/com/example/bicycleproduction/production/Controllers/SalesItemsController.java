package com.example.bicycleproduction.production.Controllers;

import com.example.bicycleproduction.production.Models.SalesItems;
import com.example.bicycleproduction.production.Services.SalesItemsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/salesItems")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class SalesItemsController {

    @Autowired
    private SalesItemsService salesItemsService;

    @GetMapping
    public List<SalesItems> getAllSalesItems() {
        return salesItemsService.getAllSalesItems();
    }

    @GetMapping("/{id}")
    public SalesItems getSalesItems(@PathVariable("id") Integer id) {
        return salesItemsService.getSalesItemsById(id);
    }

    @PostMapping
    public SalesItems createSalesItems(@RequestBody SalesItems salesItems) {
        return salesItemsService.saveSalesItems(salesItems);
    }

    @PutMapping
    public SalesItems updateSalesItems(@RequestBody SalesItems salesItems) {
        return salesItemsService.saveSalesItems(salesItems);
    }

    @DeleteMapping("/{id}")
    public void deleteSalesItems(@PathVariable("id") Integer id) {
        salesItemsService.deleteSalesItems(id);
    }
}
