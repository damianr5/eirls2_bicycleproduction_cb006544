package com.example.bicycleproduction.production.Services;

import com.example.bicycleproduction.production.Models.BillItem;
import com.example.bicycleproduction.production.Models.WarehouseSupplies;
import com.example.bicycleproduction.production.Models.Raw_material;
import com.example.bicycleproduction.production.Repositories.WarehouseSuppliesRepository;
import com.example.bicycleproduction.production.Repositories.RawMaterialRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class WarehouseSuppliesServices_implementation implements WarehouseSuppliesServices {

    @Autowired
    private WarehouseSuppliesRepository warehouseSuppliesRepository;

    @Autowired
    private RawMaterialRepository rawMaterialRepository;

    @Override
    public WarehouseSupplies saveWarehouseSupplies(WarehouseSupplies WarehouseSupplies) {
        WarehouseSupplies warehouseSupplies = warehouseSuppliesRepository.save(WarehouseSupplies);
        List<Raw_material> rawMaterials = WarehouseSupplies.getRaw_materials();
        for(Raw_material bill : rawMaterials){
            bill.setWarehouseSupplies(warehouseSupplies);
            rawMaterialRepository.save(bill);
        }
        return warehouseSupplies;
    }

    @Override
    public WarehouseSupplies getWarehouseSuppliesById(int id) {
        return warehouseSuppliesRepository.findById(id).orElse(null);
    }

    @Override
    public void deleteWarehouseSupplies(int id) {
        WarehouseSupplies user = warehouseSuppliesRepository.findById(id).orElse(null);
        warehouseSuppliesRepository.delete(user);
    }

    @Override
    public List<WarehouseSupplies> getAllWarehouseSupplies() {
        return warehouseSuppliesRepository.findAll();
    }
}



