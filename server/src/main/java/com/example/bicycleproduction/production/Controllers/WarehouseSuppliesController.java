package com.example.bicycleproduction.production.Controllers;

import com.example.bicycleproduction.production.Models.BillItem;
import com.example.bicycleproduction.production.Models.WarehouseSupplies;
import com.example.bicycleproduction.production.Models.Raw_material;
import com.example.bicycleproduction.production.Models.WarehouseSupplies;
import com.example.bicycleproduction.production.Services.BillItemServices;
import com.example.bicycleproduction.production.Services.WarehouseSuppliesServices;
import com.example.bicycleproduction.production.Services.RawMaterialServices;
import com.example.bicycleproduction.production.Services.WarehouseSuppliesServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/warehouse_supplies")
@CrossOrigin("*")
public class WarehouseSuppliesController {
    @Autowired
    private WarehouseSuppliesServices warehouseSuppliesServices;

    @Autowired
    private RawMaterialServices rawMaterialServices;

    @GetMapping
    public List<WarehouseSupplies> getAllWarehouseSupplies() {
        List<WarehouseSupplies> itemList = warehouseSuppliesServices.getAllWarehouseSupplies();
        List<WarehouseSupplies> tempItemList = new ArrayList<>();
        for(WarehouseSupplies item : itemList){
            tempItemList.add(filterBOM(item));
        }
        return tempItemList;
    }

    @GetMapping("/{id}")
    public WarehouseSupplies getWarehouseSupplies(@PathVariable("id") int id) {
        return warehouseSuppliesServices.getWarehouseSuppliesById(id);
    }

    @PostMapping
    public WarehouseSupplies createWarehouseSupplies(@RequestBody WarehouseSupplies warehouseSupplies) {
        List<Raw_material> rawMaterials = warehouseSupplies.getRaw_materials();
        List<Raw_material> temprawMaterials = new ArrayList<>();

        warehouseSupplies = warehouseSuppliesServices.saveWarehouseSupplies(warehouseSupplies);
        for (Raw_material items: rawMaterials){
            items.setWarehouseSupplies(warehouseSupplies);
            items = rawMaterialServices.saveRawMaterial(items);
            temprawMaterials.add(items);
        }

        warehouseSupplies.setRaw_materials(temprawMaterials);
        warehouseSupplies = filterBOM(warehouseSupplies);

        return warehouseSupplies;
    }

    public WarehouseSupplies filterBOM(WarehouseSupplies warehouseSupplies){
        List<Raw_material> rawMaterials = warehouseSupplies.getRaw_materials();
        List<Raw_material> temprawMaterials = new ArrayList<>();
        for (Raw_material item: rawMaterials) {
            item.setWarehouseSupplies(null);
            temprawMaterials.add(item);
        }
        warehouseSupplies.setRaw_materials(temprawMaterials);

        return warehouseSupplies;
    }

    @PutMapping
    public WarehouseSupplies updateWarehouseSupplies(@RequestBody WarehouseSupplies warehouseSupplies) {
        return warehouseSuppliesServices.saveWarehouseSupplies(warehouseSupplies);
    }

    @DeleteMapping("/{id}")
    public void deleteWarehouseSupplies(@PathVariable("id") int id) {
        warehouseSuppliesServices.deleteWarehouseSupplies(id);
    }
}

