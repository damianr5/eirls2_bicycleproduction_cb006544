package com.example.bicycleproduction.production.Services;

import com.example.bicycleproduction.production.Models.Raw_material;

import java.util.List;


public interface RawMaterialServices {

    Raw_material saveRawMaterial(Raw_material raw_material);

    Raw_material getRawMaterialById(int id);

    void deleteRawMaterial(int id);

    List<Raw_material> getAllRawMaterials();
}
