package com.example.bicycleproduction.production.Services;

import com.example.bicycleproduction.production.Models.User;

import java.util.List;


public interface UserServices {

    User saveUser(User user);

    User getUserById(int id);

    User getUserByLoginInformation(String username, String password);

    void deleteUser(int id);

    List<User> getAllUsers();
}
