package com.example.bicycleproduction.production.Services;

import com.example.bicycleproduction.production.Models.Inspection;
import com.example.bicycleproduction.production.Repositories.InspectionRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class InspectionServices_implementation implements InspectionServices {

    @Autowired
    private InspectionRespository inspection_repo;

    @Override
    public Inspection saveInspection(Inspection inspection) {
        return inspection_repo.save(inspection);
    }

    @Override
    public Inspection getInspectionById(int id) {
        return inspection_repo.findById(id).orElse(null);
    }

    @Override
    public void deleteInspection(int id) {
        Inspection user = inspection_repo.findById(id).orElse(null);
        inspection_repo.delete(user);
    }

    @Override
    public List<Inspection> getAllInspections() {
        return inspection_repo.findAll();
    }
}



