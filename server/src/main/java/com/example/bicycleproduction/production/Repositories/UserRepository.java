package com.example.bicycleproduction.production.Repositories;

import com.example.bicycleproduction.production.Models.User;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserRepository extends JpaRepository<User, Integer> {
    User findUserByUsernameAndPassword(String username, String password);

}
