package com.example.bicycleproduction.production.Services;

import com.example.bicycleproduction.production.Models.Raw_material;
import com.example.bicycleproduction.production.Repositories.RawMaterialRepository;
import com.example.bicycleproduction.production.Repositories.RawMaterialRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class RawMaterialServices_implementation implements RawMaterialServices {

    @Autowired
    private RawMaterialRepository rawMaterialRepository;

    @Override
    public Raw_material saveRawMaterial(Raw_material raw_material) {
        return rawMaterialRepository.save(raw_material);
    }

    @Override
    public Raw_material getRawMaterialById(int id) {
        return rawMaterialRepository.findById(id).orElse(null);
    }

    @Override
    public void deleteRawMaterial(int id) {
        Raw_material user = rawMaterialRepository.findById(id).orElse(null);
        rawMaterialRepository.delete(user);
    }

    @Override
    public List<Raw_material> getAllRawMaterials() {
        return rawMaterialRepository.findAll();
    }
}



