package com.example.bicycleproduction.production.Models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class BillItem {

    private @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer billId;

    private String id;
    private int prodId;
    private int qty;

    @ManyToOne
    private BillOfMaterials bom;

    public BillItem() {

    }

}
