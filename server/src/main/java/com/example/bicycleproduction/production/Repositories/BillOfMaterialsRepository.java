package com.example.bicycleproduction.production.Repositories;

import com.example.bicycleproduction.production.Models.BillOfMaterials;
import org.springframework.data.jpa.repository.JpaRepository;


public interface BillOfMaterialsRepository extends JpaRepository<BillOfMaterials, Integer> {


}
