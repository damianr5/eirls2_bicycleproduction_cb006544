package com.example.bicycleproduction.production.Services;

import com.example.bicycleproduction.production.Models.BillItem;

import java.util.List;


public interface BillItemServices {

    BillItem saveBillItem(BillItem BillItem);

    BillItem getBillItemById(int id);

    void deleteBillItem(int id);

    List<BillItem> getAllBillItems();
}
