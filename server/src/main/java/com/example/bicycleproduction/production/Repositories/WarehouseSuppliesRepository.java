package com.example.bicycleproduction.production.Repositories;

import com.example.bicycleproduction.production.Models.BillOfMaterials;
import com.example.bicycleproduction.production.Models.WarehouseSupplies;
import org.springframework.data.jpa.repository.JpaRepository;


public interface WarehouseSuppliesRepository extends JpaRepository<WarehouseSupplies, Integer> {


}
