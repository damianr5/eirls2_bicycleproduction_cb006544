package com.example.bicycleproduction.production.Repositories;

import com.example.bicycleproduction.production.Models.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ScheduleRepository extends JpaRepository<Schedule, Integer> {


}
