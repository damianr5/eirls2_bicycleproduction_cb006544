package com.example.bicycleproduction.production.Services;

import com.example.bicycleproduction.production.Models.WarehouseSupplies;

import java.util.List;


public interface WarehouseSuppliesServices {

    WarehouseSupplies saveWarehouseSupplies(WarehouseSupplies warehouseSupplies);

    WarehouseSupplies getWarehouseSuppliesById(int id);

    void deleteWarehouseSupplies(int id);

    List<WarehouseSupplies> getAllWarehouseSupplies();
}
