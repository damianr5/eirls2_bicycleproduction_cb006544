package com.example.bicycleproduction.production.Repositories;

import com.example.bicycleproduction.production.Models.Inspection;
import org.springframework.data.jpa.repository.JpaRepository;


public interface InspectionRespository extends JpaRepository<Inspection, Integer> {


}
