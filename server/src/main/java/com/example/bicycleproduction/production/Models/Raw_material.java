package com.example.bicycleproduction.production.Models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Raw_material {

    private @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer rawId;

    private String name;
    private int id;
    private int quantity;

    @ManyToOne
    private WarehouseSupplies warehouseSupplies;

    public Raw_material() {

    }

}
