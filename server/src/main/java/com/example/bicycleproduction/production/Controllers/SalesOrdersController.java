package com.example.bicycleproduction.production.Controllers;

import com.example.bicycleproduction.production.Models.SalesItems;
import com.example.bicycleproduction.production.Models.SalesOrders;
import com.example.bicycleproduction.production.Services.SalesItemsService;
import com.example.bicycleproduction.production.Services.SalesOrdersServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/salesOrders")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class SalesOrdersController {

    @Autowired
    private SalesOrdersServices salesOrdersServices;

    @Autowired
    private SalesItemsService salesItemsService;
    @GetMapping
    public List<SalesOrders> getAllSalesOrders() {
        List<SalesOrders> ordersList = salesOrdersServices.getAllSalesOrders();
        List<SalesOrders> tempOrdersList = new ArrayList<>();
        for(SalesOrders order : ordersList){
            tempOrdersList.add(filterOrders(order));
        }
        return tempOrdersList;
    }

    @GetMapping("/{id}")
    public SalesOrders getSalesOrder(@PathVariable("id") Integer id) {
        return filterOrders(salesOrdersServices.getSalesOrdersById(id));
    }

    @PostMapping("/completedOrder")
    public SalesOrders getSalesOrders(@RequestBody SalesOrders salesOrders){
        System.out.println("object" +salesOrders);
        List<SalesOrders> salesOrdersList = salesOrdersServices.getAllSalesOrders();
        if (salesOrdersList.size()!=0){
            for (int i=0;i<salesOrdersList.size(); i++){
                if (salesOrders.getOrder_id().equals(salesOrdersList.get(i).getOrder_id())){
                    salesOrdersList.get(i).setOrder_status(salesOrders.getOrder_status());
                    salesOrdersServices.saveSalesOrders(salesOrdersList.get(i));
                }
            }
        }
        return salesOrders;
    }

    @PostMapping
    public SalesOrders createSalesOrder(@RequestBody SalesOrders salesOrders) {
        List<SalesItems>  salesItems = salesOrders.getSalesItems();
        List<SalesItems> tempSalesItems = new ArrayList<>();

        salesOrders = salesOrdersServices.saveSalesOrders(salesOrders);
        for (SalesItems items: salesItems){
            items.setSalesOrders(salesOrders);
            items = salesItemsService.saveSalesItems(items);
            tempSalesItems.add(items);
        }

        salesOrders.setSalesItems(tempSalesItems);
        salesOrders = filterOrders(salesOrders);

        return salesOrders;
    }

    @PostMapping("salesOrdersUpdate")
    public SalesOrders updatedOrders(@RequestBody SalesOrders salesOrders){
        List<SalesOrders> salesOrdersList = salesOrdersServices.getAllSalesOrders();
        if(salesOrdersList.size()!=0){
            for(int i = 0; i< salesOrdersList.size(); i++){
                if(salesOrders.getOrder_id().equals(salesOrdersList.get(i).getOrder_id())){
                    salesOrdersList.get(i).setOrder_due_date(salesOrders.getOrder_due_date());
                    salesOrdersList.get(i).setOrder_status(salesOrders.getOrder_status());
                    salesOrdersServices.saveSalesOrders(salesOrdersList.get(i));
                }
            }
        }
        return null;
    }


    public SalesOrders filterOrders(SalesOrders salesOrders){
        List<SalesItems> salesItems = salesOrders.getSalesItems();
        List<SalesItems> tempItems = new ArrayList<>();
        for (SalesItems item: salesItems) {
            item.setSalesOrders(null);
            tempItems.add(item);
        }
        salesOrders.setSalesItems(tempItems);

        return salesOrders;
    }

    @PutMapping
    public SalesOrders updateSalesOrder(@RequestBody SalesOrders salesOrders) {
        return salesOrdersServices.saveSalesOrders(salesOrders);
    }

    @DeleteMapping("/{id}")
    public void deleteSalesOrder(@PathVariable("id") Integer id) {
        salesOrdersServices.deleteSalesOrders(id);
    }
}
