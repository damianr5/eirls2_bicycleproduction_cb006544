package com.example.bicycleproduction.production.Repositories;

import com.example.bicycleproduction.production.Models.SalesItems;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SalesItemsRepo extends JpaRepository<SalesItems,Integer > {
}
