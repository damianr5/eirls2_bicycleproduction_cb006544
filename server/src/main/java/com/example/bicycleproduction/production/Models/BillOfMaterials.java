package com.example.bicycleproduction.production.Models;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class BillOfMaterials {

    private @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    private String order_Id;
    private String status;


    @OneToMany(mappedBy = "bom", cascade = CascadeType.ALL)
    private List<BillItem> BillItems;

    public BillOfMaterials() {

    }

}
