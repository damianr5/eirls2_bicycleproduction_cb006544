package com.example.bicycleproduction.production.Controllers;

import com.example.bicycleproduction.production.Models.User;
import com.example.bicycleproduction.production.Services.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserServices userServices;

    @GetMapping
    public List<User> getAllUsers() {
        return userServices.getAllUsers();
    }

    @GetMapping("/{id}")
    public User getUser(@PathVariable("id") int id) {
        return userServices.getUserById(id);
    }

    @PostMapping
    public User createUser(@RequestBody User user) {
        return userServices.saveUser(user);
    }

    @PutMapping
    public User updateUser(@RequestBody User user) {
        return userServices.saveUser(user);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable("id") int id) {
        userServices.deleteUser(id);
    }
}

