package com.example.bicycleproduction.production.Models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Schedule {

    private @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    private String order_id;
    private String start_date;
    private String end_date;
    private int employee_amount;

    public Schedule() {

    }
}
