package com.example.bicycleproduction.production.Services;

import com.example.bicycleproduction.production.Models.BillOfMaterials;

import java.util.List;


public interface BillOfMaterialsServices {

    BillOfMaterials saveBillOfMaterials(BillOfMaterials BillOfMaterials);

    BillOfMaterials getBillOfMaterialsById(int id);

    void deleteBillOfMaterials(int id);

    List<BillOfMaterials> getAllBillOfMaterials();
}
