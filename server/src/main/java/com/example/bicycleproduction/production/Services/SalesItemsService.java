package com.example.bicycleproduction.production.Services;

import com.example.bicycleproduction.production.Models.SalesItems;

import java.util.List;

public interface SalesItemsService {

    SalesItems saveSalesItems(SalesItems salesItems);

    void deleteSalesItems(Integer id);

    SalesItems getSalesItemsById (Integer id);

    List<SalesItems> getAllSalesItems();
}
