package com.example.bicycleproduction.production.Models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class User {

    private @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    private String username;
    private String password;

    public User(){

    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
