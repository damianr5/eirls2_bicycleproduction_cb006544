package com.example.bicycleproduction.production.Models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Inspection {

    private @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    private String date_and_time;
    private String status;

    public Inspection() {

    }

    public Inspection(String date_and_time, String status) {
        this.date_and_time = date_and_time;
        this.status = status;
    }
}
