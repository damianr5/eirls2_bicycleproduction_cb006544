package com.example.bicycleproduction.production.Controllers;

import com.example.bicycleproduction.production.Models.BillItem;
import com.example.bicycleproduction.production.Services.BillItemServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/bill_item")
public class BillItemController {
    @Autowired
    private BillItemServices billItemService;

    @GetMapping
    public List<BillItem> getAllBillItems() {
        return billItemService.getAllBillItems();
    }

    @GetMapping("/{id}")
    public BillItem getBillItem(@PathVariable("id") int id) {
        return billItemService.getBillItemById(id);
    }

    @PostMapping
    public BillItem createBillItem(@RequestBody BillItem BillItem) {
        return billItemService.saveBillItem(BillItem);
    }

    @PutMapping
    public BillItem updateBillItem(@RequestBody BillItem BillItem) {
        return billItemService.saveBillItem(BillItem);
    }

    @DeleteMapping("/{id}")
    public void deleteBillItem(@PathVariable("id") int id) {
        billItemService.deleteBillItem(id);
    }
}

