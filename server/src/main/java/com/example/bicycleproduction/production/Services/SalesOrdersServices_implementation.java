package com.example.bicycleproduction.production.Services;

import com.example.bicycleproduction.production.Models.SalesOrders;
import com.example.bicycleproduction.production.Repositories.SalesOrdersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SalesOrdersServices_implementation implements SalesOrdersServices {

    @Autowired
    private SalesOrdersRepository salesOrdersRepository;


    @Override
    public SalesOrders saveSalesOrders(SalesOrders salesOrders) {
        return salesOrdersRepository.save(salesOrders);
    }

    @Override
    public SalesOrders getSalesOrdersById(Integer id) {
        return salesOrdersRepository.findById(id).orElse(null);
    }

    @Override
    public void deleteSalesOrders(Integer id) {
        SalesOrders salesOrders = salesOrdersRepository.findById(id).orElse(null);
        salesOrdersRepository.delete(salesOrders);

    }

    @Override
    public List<SalesOrders> getAllSalesOrders() {
        return salesOrdersRepository.findAll();
    }
}
