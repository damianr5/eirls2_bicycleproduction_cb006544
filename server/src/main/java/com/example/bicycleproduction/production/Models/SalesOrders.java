package com.example.bicycleproduction.production.Models;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class SalesOrders {


    private @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    private String order_id;
    private String order_due_date;
    private String order_status;
    private String order_placement_date;

    @OneToMany(mappedBy="salesOrders", cascade = CascadeType.ALL)
    private List<SalesItems> salesItems;

    @OneToOne
    private BillOfMaterials billOfMaterials;



    public SalesOrders() {

    }
}
