package com.example.bicycleproduction.production.Services;

import com.example.bicycleproduction.production.Models.BillItem;
import com.example.bicycleproduction.production.Repositories.BillItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class BillItemServices_implementation implements BillItemServices {

    @Autowired
    private BillItemRepository bill_item_repo;

    @Override
    public BillItem saveBillItem(BillItem BillItem) {
        return bill_item_repo.save(BillItem);
    }

    @Override
    public BillItem getBillItemById(int id) {
        return bill_item_repo.findById(id).orElse(null);
    }

    @Override
    public void deleteBillItem(int id) {
        BillItem user = bill_item_repo.findById(id).orElse(null);
        bill_item_repo.delete(user);
    }

    @Override
    public List<BillItem> getAllBillItems() {
        return bill_item_repo.findAll();
    }
}



