package com.example.bicycleproduction.production.Services;

import com.example.bicycleproduction.production.Models.Schedule;

import java.util.List;


public interface ScheduleServices {

    Schedule saveSchedule(Schedule schedule);

    Schedule getScheduleById(int id);

    void deleteSchedule(int id);

    List<Schedule> getAllSchedules();
}
