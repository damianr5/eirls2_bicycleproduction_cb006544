package com.example.bicycleproduction.production.Repositories;

import com.example.bicycleproduction.production.Models.BillItem;
import org.springframework.data.jpa.repository.JpaRepository;


public interface BillItemRepository extends JpaRepository<BillItem, Integer> {


}
