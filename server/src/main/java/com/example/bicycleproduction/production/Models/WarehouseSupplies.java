package com.example.bicycleproduction.production.Models;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class WarehouseSupplies {

    private @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    private String order_Id;
    private String status;
    private String bom_id;

    @OneToMany(mappedBy = "warehouseSupplies", cascade = CascadeType.ALL)
    private List<Raw_material> raw_materials;

    public WarehouseSupplies() {

    }

}
