package com.example.bicycleproduction.production.Controllers;

import com.example.bicycleproduction.production.Models.Raw_material;
import com.example.bicycleproduction.production.Services.RawMaterialServices;
import com.example.bicycleproduction.production.Services.RawMaterialServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/raw_material")
public class RawMaterialController {
    @Autowired
    private RawMaterialServices rawMaterialServices;

    @GetMapping
    public List<Raw_material> getAllRawMaterials() {
        return rawMaterialServices.getAllRawMaterials();
    }

    @GetMapping("/{id}")
    public Raw_material getRawMaterial(@PathVariable("id") int id) {
        return rawMaterialServices.getRawMaterialById(id);
    }

    @PostMapping
    public Raw_material createRawMaterial(@RequestBody Raw_material raw_material) {
        return rawMaterialServices.saveRawMaterial(raw_material);
    }

    @PutMapping
    public Raw_material updateRawMaterial(@RequestBody Raw_material raw_material) {
        return rawMaterialServices.saveRawMaterial(raw_material);
    }

    @DeleteMapping("/{id}")
    public void deleteRawMaterial(@PathVariable("id") int id) {
        rawMaterialServices.deleteRawMaterial(id);
    }
}

