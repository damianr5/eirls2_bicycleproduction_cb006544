package com.example.bicycleproduction.production.Repositories;

import com.example.bicycleproduction.production.Models.SalesOrders;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SalesOrdersRepository extends JpaRepository <SalesOrders, Integer>{
}
