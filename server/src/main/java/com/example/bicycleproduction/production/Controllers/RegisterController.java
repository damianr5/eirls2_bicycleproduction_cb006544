package com.example.bicycleproduction.production.Controllers;

import com.example.bicycleproduction.production.Models.User;
import com.example.bicycleproduction.production.Services.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/adminRegistration")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class RegisterController {

    @Autowired
    private UserServices userServices;

    @PostMapping
    public User createUser(@RequestBody User user){
        return userServices.saveUser(user);
    }
}
