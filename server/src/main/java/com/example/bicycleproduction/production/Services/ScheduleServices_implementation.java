package com.example.bicycleproduction.production.Services;

import com.example.bicycleproduction.production.Models.Schedule;
import com.example.bicycleproduction.production.Repositories.ScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ScheduleServices_implementation implements ScheduleServices {

    @Autowired
    private ScheduleRepository schedule_repo;

    @Override
    public Schedule saveSchedule(Schedule schedule) {
        return schedule_repo.save(schedule);
    }

    @Override
    public Schedule getScheduleById(int id) {
        return schedule_repo.findById(id).orElse(null);
    }

    @Override
    public void deleteSchedule(int id) {
        Schedule user = schedule_repo.findById(id).orElse(null);
        schedule_repo.delete(user);
    }

    @Override
    public List<Schedule> getAllSchedules() {
        return schedule_repo.findAll();
    }
}



